FROM centos:7
MAINTAINER "Tristan Lins" <tristan@lins.io>

COPY install-temurin.sh /bin

# Install required tools
RUN set -x \
    && cd / \
    && yum install -y centos-release-scl wget curl \
    && wget -nv https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm \
    && rpm -i epel-release-latest-7.noarch.rpm \
    && rm epel-release-latest-7.noarch.rpm \
    && yum-config-manager --enable rhel-server-rhscl-7-rpms \
    && yum install -y git devtoolset-7 devtoolset-7-binutils devtoolset-7-gcc devtoolset-7-gcc-c++ glibc glibc-devel tar openssl-devel \
    && scl enable devtoolset-7 bash \
    && yum clean all \
    && rm -rf /var/cache/yum

# Set PATH, because "scl enable" does not have any effects to "docker build"
ENV PATH $PATH:/opt/rh/devtoolset-7/root/usr/bin

# Install cmake 3.21
RUN set -x \
    && DIR=$(mktemp -d) \
    && cd "$DIR" \
    && wget -nv --no-check-certificate http://www.cmake.org/files/v3.21/cmake-3.21.2.tar.gz -O cmake-3.21.2.tar.gz \
    && tar xf cmake-3.21.2.tar.gz \
    && cd cmake-3.21.2 \
    && ./configure \
    && make -j8 \
    && make install \
    && cd / \
    && rm -rf "$DIR"

# Install Java JDK 8
RUN set -x \
    && install-temurin.sh -f 8 -o linux -a x64 \
    && cd /usr/local/include \
    && ln -s /opt/java8/include/*.h /opt/java8/include/linux/*.h .

# Setup binaries and java home path
ENV PATH /opt/java8/bin:$PATH
ENV HOME /tmp
ENV JAVA_HOME /opt/java8

# Check java and gradle work properly
RUN set -x \
    && java -version

# Install make script
COPY make.sh /

# Run configuration
WORKDIR /jsass
CMD ["/make.sh"]
